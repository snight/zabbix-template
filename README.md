# Zabbix-Template
基于zabbix 5.2的监控模板

- 博科SAN交换机
- 华三万兆交换机
- DELL DS存储
- IBM 刀箱AMM
- 锐捷交换机
- DELL机架服务器iDRAC
- MySQL Slave 状态检查
  1. Zabbix客户端（mysql从服务器）主机必须安装jq。如果没有，请使用安装<br>
  `yum install jq`
  
  2. 在/etc/zabbix/zabbix_agentd.d目录下，创建`.my.cnf`文件， 确保zabbix用户对mysql密码文件有读权限<br>
    `[client]`<br>
    `user = zabbix`<br>
    `password = password`<br>
  3. 赋予zabbix用户访问权限<br>
    `chown zabbix .my.cnf && chmod 400 .my.cnf`<br>
  4. 在/etc/zabbix/zabbix_agentd.d目录下创建`userparameter_slave_status.conf`文件，添加以下行<br>
  `UserParameter=Mysql.Slave-Status, mysql --defaults-file=/etc/zabbix/zabbix_agentd.d/.my.cnf --defaults-group-suffix=_monitoring -e  "show slave status \G" | sed -e "s/^\s*//g" | sed -e "s/:\s*/:/g" |  jq -c  '.  | split("\n")[1:-1]  | map (split(":") | {(.[0]) : .[1]}  ) | add  ' -R -s`<br>
  5. 如果无法安装jq，则可以使用下面的脚本替换<br>
  `UserParameter=Mysql.Slave-Status, mysql --defaults-extra-file=/etc/zabbix/zabbix_agentd.d/.my.cnf -h127.0.0.1 -e  "show slave status \G" 2> /dev/null | awk '/Seconds/||/Slave/||/Log/ {print $1,$2}' | awk -F":" 'BEGIN{ORS="";print "{"}{if(NR>1)print ",\""$1"\":\""$2"\"";else print "\""$1"\":\""$2"\""}END{ORS="\n";print "}"}'`

